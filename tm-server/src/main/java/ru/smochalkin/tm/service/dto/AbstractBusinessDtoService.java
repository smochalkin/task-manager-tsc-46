package ru.smochalkin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.dto.AbstractBusinessEntityDto;

public abstract class AbstractBusinessDtoService<E extends AbstractBusinessEntityDto> extends AbstractDtoService<E> implements IBusinessService<E> {

    public AbstractBusinessDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

}
