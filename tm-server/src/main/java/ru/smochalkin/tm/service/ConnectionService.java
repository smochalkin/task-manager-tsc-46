package ru.smochalkin.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;


    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(
            @NotNull final IPropertyService propertyService
    ) {
        this.propertyService = propertyService;
        this.entityManagerFactory = factory();
    }

    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(Environment.URL, propertyService.getDatabaseUrl());
        settings.put(Environment.USER, propertyService.getDatabaseUser());
        settings.put(Environment.PASS, propertyService.getDatabasePassword());
        settings.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getUseSecondLevelCache());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getCacheRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheProviderConfig());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getCacheRegionFactory());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDto.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDto.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(SessionDto.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(UserDto.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
