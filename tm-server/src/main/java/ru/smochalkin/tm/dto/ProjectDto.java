package ru.smochalkin.tm.dto;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDto extends AbstractBusinessEntityDto {
}
