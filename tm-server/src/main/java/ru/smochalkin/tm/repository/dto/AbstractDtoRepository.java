package ru.smochalkin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.AbstractEntityDto;

import javax.persistence.EntityManager;

public abstract class AbstractDtoRepository<E extends AbstractEntityDto> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

}
