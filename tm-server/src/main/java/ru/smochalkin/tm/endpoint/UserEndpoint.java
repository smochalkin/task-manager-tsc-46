package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.endpoint.IUserEndpoint;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;
import ru.smochalkin.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public UserDto findUserBySession(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getUserService().findById(sessionDto.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result userSetPassword(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "password") @Nullable final String password
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getUserService().setPassword(sessionDto.getUserId(), password);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result userUpdate(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getUserService().updateById(sessionDto.getUserId(), firstName, lastName, middleName);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
