package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractSystemCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    @NotNull
    public Collection<AbstractSystemCommand> getArguments() {
        return arguments.values();
    }

    @Override
    @NotNull
    public List<String> getCommandNames() {
        return new ArrayList<>(commands.keySet());
    }

    @Override
    @NotNull
    public List<String> getCommandArgs() {
        return new ArrayList<>(arguments.keySet());
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull String name) {
        return commands.get(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@NotNull String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        if (command instanceof AbstractSystemCommand) {
            @NotNull final AbstractSystemCommand systemCommand;
            systemCommand = (AbstractSystemCommand) command;
            @Nullable final String arg = systemCommand.arg();
            if (arg != null) arguments.put(arg, systemCommand);
        }
        @Nullable final String name = command.name();
        if (name != null) commands.put(name, command);
    }

}
