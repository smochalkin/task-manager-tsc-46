package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.endpoint.ProjectDto;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.SortNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectShowListCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display project list.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter sort option from list:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final List<ProjectDto> projects;
        String sortName = TerminalUtil.nextLine();
        if (sortName.isEmpty()) {
            projects = serviceLocator.getProjectEndpoint().findProjectAll(serviceLocator.getSession());
        } else {
            projects = serviceLocator.getProjectEndpoint().findProjectAllSorted(serviceLocator.getSession(), sortName);
        }
        int index = 1;
        for (ProjectDto project : projects) {
            System.out.println(index++ + ". " + project.getId() + "|" + project.getName() + "|" + project.getStatus());
        }
    }

}
