package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-clear";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove all tasks of current user.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        @NotNull final Result result = serviceLocator.getTaskEndpoint().clearTasks(serviceLocator.getSession());
        printResult(result);
    }

}
